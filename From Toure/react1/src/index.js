import React from "react";
import { createRoot } from "react-dom/client";
import "bootstrap/dist/css/bootstrap.css";


const rootElement = document.getElementById("root");
const root = createRoot(rootElement);
//const myh4=React.createElement('h4', null,"hello world");
//const myul=React.createElement('ul', null,
//React.createElement('li', null, "one"),
//React.createElement('li', null, "Two"),
//React.createElement('li', null, "Three")
//)
//const mydiv=React.createElement('div', null, myh4,myul);

//ici le Mydiv est ecrit comme composant
const MyPlayer=({num, nom, goal, team, assis}) =>(
  <div>
    <h4 className="alert alert-success display-5">Player {num}</h4>
    <ul type="square">
      <li className="alert alert-warning">Name: {nom}</li>
      <li className="alert alert-danger">Goal: {goal}</li>
      <li className="alert alert-success">Team: {team}</li>
      <li className="alert alert-success">Assis: {assis}</li>
      <li className="alert alert-success">
        Trophée: {""} 
        {goal + assis >=70
        ? "give him it's BO"
        : "work hard"}
      </li>
    </ul>
    </div>
    );
root.render(
  <div>
    <MyPlayer num={1} nom="sadio mané" goal={50} team="liverpool" assis={20}/>
    <MyPlayer num={2} nom="Messi" goal={30} team="Paris" assis={30}/>
  </div>
);

