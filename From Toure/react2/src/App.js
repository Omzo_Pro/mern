import { MyNumber } from "./MyNumber";
import { MyText } from "./MyText";
export default function App() {
  return (
    <div className="App">
      <MyNumber num={4 * 2} />
      <MyText text="UVS" />
    </div>
  );
}
