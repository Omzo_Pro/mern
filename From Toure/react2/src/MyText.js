import React from "react";
import "bootstrap/dist/css/bootstrap.css";
export const MyText = (props) => (
  <h3 className="alert alert-success">My Text is {props.text}</h3>
);
