import "bootstrap/dist/css/bootstrap.css";
import { MyNumber } from "./MyNumber";
import { MyText } from "./MyText";
export default function App() {
  return (
    <div className="App">
      <MyNumber num={4 * 2} />
      <MyNumber num={43} />
      <MyText text={["UVS", "UGB", "UCAD"]} />
    </div>
  );
}
