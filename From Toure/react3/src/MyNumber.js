import React from "react";

export const MyNumber = (props) => {
  let isPair;
  let classes;
  [isPair, classes] =
    props.num % 2 !== 0
      ? ["Impair", "alert alert-warning display-5"]
      : ["Pair", "alert alert-danger display-5"];

  return (
    <h3 className={classes}>
      The Number :{props.num} is {isPair}
    </h3>
  );
};
