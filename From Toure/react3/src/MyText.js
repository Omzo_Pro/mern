import React from "react";

export const MyText = (props) => (
  <div className="alert alert-info">
    My Text
    <h4 className="alert alert-success display-6 p-3">
      {props.text.map((name) => (
        <div>{`${name} contains ${name.length} letters`}</div>
      ))}
    </h4>
  </div>
);
