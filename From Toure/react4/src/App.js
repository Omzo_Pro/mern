import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import { Avatar } from "./Avatar";

let names = ["Omzo", "Momo", "Junior", "Yamzo", "Azou"];
function reverseNames() {
  names.reverse();
  ReactDOM.render(<App />, document.querySelector("#root"));
}
function promoteName(name) {
  names = [name, ...names.filter((val) => val !== name)];
  ReactDOM.render(<App />, document.querySelector("#root"));
}
export default function App() {
  return (
    <table className="table table-sm table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>letters</th>
          <th>G. Action</th>
          <th>S. Action</th>
          <th>S. Action</th>
        </tr>
      </thead>
      <tbody>
        {names.map((name, index) => (
          <tr key={name}>
            <Avatar
              name={name}
              index={index}
              reverseCallback={reverseNames}
              promoteCallback={promoteName}
            />
          </tr>
        ))}
      </tbody>
    </table>
  );
}
