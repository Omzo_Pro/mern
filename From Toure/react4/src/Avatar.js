import { CallbackButton } from "./CallbackButton";

export const Avatar = (props) => (
  <>
    <td>{props.index + 1}</td>
    <td>{props.name}</td>
    <td>{props.name.length}</td>
    <td>
      <CallbackButton
        theme="danger"
        callback={props.reverseCallback}
        text="reverse"
      />
    </td>
    <td>
      <CallbackButton
        theme="info"
        callback={() => props.promoteCallback(props.name)}
        text="promote"
      />
    </td>
    <td>
      <button
        className="btn btn-info btn-sm"
        onClick={() => props.promoteCallback(props.name)}
      >
        Promote
      </button>
    </td>
  </>
);
