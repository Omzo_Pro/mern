import React from "react";

export const CallbackButton = (props) => (
  <button className={`btn btn-${props.theme} btn-sm`} onClick={props.callback}>
    {props.text}
  </button>
);
