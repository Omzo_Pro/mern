import React from 'react';
import './App.css';

const App = () => {
  return <h1 title="this works">This is React!</h1>; //=>React.createElement('H1', {title:'this works'}, 'This is React!')
};

export default App;
